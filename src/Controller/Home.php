<?php

namespace App\Controller;

use App\Services\ImageFetcherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Home extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     *
     * @param ImageFetcherService $fetcher
     * @param Request $request
     * @return Response
     */
    public function __invoke(ImageFetcherService $fetcher, Request $request): Response
    {
        return $this->render('default/index.html.twig', ['images' => $fetcher->importImages()]);
    }
}
