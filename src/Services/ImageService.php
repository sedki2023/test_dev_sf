<?php

namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;

class ImageService
{
    /**
     * @param $imagesPages
     * @return array
     */
    public function retrieveImagesFromPages($imagesPages): array
    {
        $imagesPages = array_unique(array_merge([], ...$imagesPages));

        $images = array();
        foreach ($imagesPages as $image) {
            try {
                $crawler = new Crawler(file_get_contents($image));
                $query = (strpos($image, 'commitstrip.com') !== false)
                    ? '//img[contains(@class,"size-full")]'
                    : '//img';
                $imageLinks = $crawler->filterXpath($query)->extract(array('src'));
                if (count($imageLinks) > 0 && $imageLinks[0] !== '') {
                    $images[] = $imageLinks[0];
                }
            } catch (\Exception $e) {
                // Handle the exception
            }
        }

        return $images;
    }

}
