<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewsApiImageFetcher implements ImageFetcherInterface
{
    private $client;
    private $apiLink;

    public function __construct(HttpClientInterface $client, $apiLink)
    {
        $this->client = $client;
        $this->apiLink = $apiLink;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function doImport(): array
    {
        // recpere liens api json avec image
        $response = $this->client->request(
            'GET',
            $this->apiLink
        );
        $articles = $response->toArray()['articles'];
        $images = [];
        foreach ($articles as $article) {
            if (empty($article['urlToImage'])) {
                continue;
            }
            $images[] = $article['url'];
        }

        return $images;
    }
}
