<?php

namespace App\Services;

class RSSImageFetcher implements ImageFetcherInterface
{
    private $rssLink;

    public function __construct($rssLink)
    {
        $this->rssLink = $rssLink;
    }

    /**
     * @return array
     */
    public function doImport(): array
    {
        $imageLinks = [];
        // recupere liens flux rss avec images
        try {
            $data = file_get_contents($this->rssLink);
            if ($data) {
                $xmlData = simplexml_load_string(file_get_contents($this->rssLink), 'SimpleXMLElement', LIBXML_NOCDATA);
                $channel = $xmlData->channel;
                foreach ($channel->item as $item) {
                    $content = (string) $item->children('content', true);

                    if (strpos($content, 'jpg') !== false || strpos($content, 'JPG') !== false || strpos($content, 'GIF') !== false || strpos($content, 'gif') !== false || strpos($content, 'PNG') !== false || strpos($content, '.png') !== false) {
                        $imageLinks[] = (string) $item->link;
                    }
                }
            }
        } catch (\Exception $e) {
        }
        return $imageLinks;
    }
}
