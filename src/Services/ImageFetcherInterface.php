<?php

namespace App\Services;

interface ImageFetcherInterface
{
    public function doImport(): array;
}
