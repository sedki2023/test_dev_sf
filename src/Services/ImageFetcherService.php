<?php

namespace App\Services;

class ImageFetcherService
{
    private $fetchers;
    private $imagesService;

    public function __construct(iterable $fetchers, ImageService $imagesService)
    {
        $this->fetchers = $fetchers;
        $this->imagesService = $imagesService;
    }

    public function importImages(): array
    {
        /** @var ImageFetcherInterface $fetcher */
        $images = [];
        foreach ($this->fetchers as $fetcher) {
            $images[] = $fetcher->doImport();
        }

        return $this->imagesService->retrieveImagesFromPages($images);
    }
}
